const jwt = require('jsonwebtoken');

const secret = "CourseBookingAPI";

//[SECTION] JSON Web Token
    //JWT is a way of securely passing information form the server to the frontend or to other parts of the server.

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };

    return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {

    let token = req.headers.authorization;

    if(typeof token !== "undefined"){
        
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return res.send({auth: "failed"});
            } else {
                next();
            }
        });
    } else {
        return res.send({auth: "failed"});
    }
};

module.exports.decode = (token) => {
    if(typeof token !== "undefined"){

        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) =>{
            if(err){
                return null;
            } else {
                return jwt.decode(token, {complete: true}).payload;
            } 
        });
    } else {
        return null;
    }
};

//[ACTIVITY S39 OUTPUT]
// module.exports.verifyAdmin = (req, res, next) => {

//     let token = req.headers.authorization;

//     if(typeof token !== "undefined"){
        
//         token = token.slice(7, token.length);

//         return jwt.verify(token, secret, (err, data) => {

//             let isAdmin = jwt.decode(token, {complete: true}).payload.isAdmin;

//             if(err){
//                 return res.send({auth: "failed"});
//             } else if(isAdmin != true){
//                 return res.send({auth: "failed: User is not an admin."});
//             } else {
//                 next();
//             }
//         });
//     } else {
//         return res.send({auth: "failed"});
//     }
// };

//Sir's Solution - only used the auth.verify middleware

