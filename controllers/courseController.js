const Course = require('../models/Course');

module.exports.addCourse = (data) => {

    if(data.isAdmin){
        let newCourse = new Course({
            name: data.course.name,
            description: data.course.description,
            price: data.course.price
        });
    
        return newCourse.save().then((course, error) => {
            if(error){
                return false
            } else {
                return {
                    message: "New course successfully created!"
                }
            }
        });
    }

    let message = Promise.resolve({
        message: "User must be an admin to access this!"
    })

    return message.then(value => {
        return value;
    })

};


module.exports.getAllCourse = () => {
    return Course.find({}).then(result => {
        return result;
    });
};

module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    });
};

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    });
};

module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.id, updatedCourse).then((course, error) => {
        if(error){
            return false;
        } else {
            let message = `Successfully updated course Id - "${reqParams.id}" \n\n ${JSON.stringify(updatedCourse)}`

            return message;
        }
    });
}

// module.exports.archiveCourse = (reqParams) => {
    
//     return Course.findByIdAndUpdate(reqParams.id, {isActive: false}).then(result => {
//         return {
//             isActive: result.isActive};
//     });
// };

module.exports.archiveCourse = (reqParams) => {
    let updatedActiveField = {
        isActive: false
    };

    return Course.findByIdAndUpdate(reqParams.id, updatedActiveField).then((course, error) => {
        if(error){
            return false;
        } else {
            return true
        }
    });
};