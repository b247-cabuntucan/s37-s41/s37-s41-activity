const User = require('../models/User');
const Course = require('../models/Course')
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0 ){
            return true
        } else {
            return false
        }
    });
};

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10) // to encrypt the password
    });

    return newUser.save().then((user, error) => {
        if(error) {
            return false
        } else {
            return true
        };
    });
};

module.exports.loginUser = (reqBody) => {
    //We use the "findOne" method instead of the "find" method which return all records that match the search criteria.
    //The "findOne" method return the first record in the collection that mathes the search criteria
    return User.findOne({email: reqBody.email}).then(result => {
        // User does not exist
        if(result == null){
            return false
        // User exists
        } else {
            //The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrived form the database and it returns "true" or "false" value depending on the result. A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){
                //Generate an access token
                return {access: auth.createAccessToken(result)};
            } else {
                return false;
            };
        };
    });
};

//[ACTIVITY 38 OUTPUT] 
module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        result.password = "";
        return result
    });
};

module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.enrollments.push({courseID: data.courseId});

        return user.save().then((user, error) => {
            if(error){
                return false;
            } else {
                return true;
            }
        });
    });

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userId: data.userId});

        return course.save().then((course, error) => {
            if(error){
                return false;
            } else {
                return true;
            }
        });
    });

    if(isUserUpdated && isCourseUpdated){
        return true;
    } else {
        return false;
    }
};
